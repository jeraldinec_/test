function validationSignIn() {
    var validEmail = true, validPass = true;

    emailSignIn = document.getElementById("emailSignIn").value;
    if ( emailSignIn == null || emailSignIn.length == 0 || /^\s+$/.test(emailSignIn)) {
        $("#emailSignInRequired").css('display','block');
        validEmail = false;
    }

    passSignIn = document.getElementById("passSignIn").value;
    if ( passSignIn == null || passSignIn.length == 0 || /^\s+$/.test(passSignIn)) {
        $("#passSignInRequired").css('display','block');
        validPass = false;
    }

    return validEmail && validPass;

}

function validationRegister() {
    var validName = true, validEmailRegister = true, validPhone = true, validPassword = true;

    nameRegister = document.getElementById("name").value;
    if ( nameRegister == null || nameRegister.length == 0) {
        $("#nameRequired").css('display','block');
        validName = false;
    }
    else {
        if (! /^[A-Za-z]+$/.test(nameRegister)) {
            $("#namePattern").css('display','block');
            validName = false;
        }
        else{
            $("#namePattern").css('display','none');
        }
        $("#nameRequired").css('display','none');
    }

    email = document.getElementById("email").value;
    if ( email == null || email.length == 0) {
        $("#emailRequired").css('display','block');
        validEmailRegister = false;
    }
    else {
        if (! /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
            $("#emailPattern").css('display','block');
            validEmailRegister = false;
        }
        else{
            $("#emailPattern").css('display','none');
        }
        $("#emailRequired").css('display','none');
    }

    phone = document.getElementById("phone").value;
    if ( phone == null || phone.length == 0) {
        $("#phoneRequired").css('display','block');
        validPhone = false;
    }
    else {
        if (! /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(phone)) {
            $("#phonePattern").css('display','block');
            validPhone = false;
        }
        else{
            $("#phonePattern").css('display','none');
        }
        $("#phoneRequired").css('display','none');
    }

    password = document.getElementById("password").value;
    if ( password == null || password.length == 0) {
        $("#passwordRequired").css('display','block');
        validPassword = false;
    }
    else {
        if (! /^(?=.*?[A-Z])(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[^\w\s]).{7,}$/.test(password)) {
            $("#passwordPattern").css('display','block');
            validPassword = false;
        }
        else{
            $("#passwordPattern").css('display','none');
        }
        $("#passwordRequired").css('display','none');
    }

    return validName && validEmailRegister && validPhone && validPassword;

}

function validationSignInMobile() {
    var validEmail = true, validPass = true;

    emailSignInMobile = document.getElementById("emailSignIn-mobile").value;
    if ( emailSignInMobile == null || emailSignInMobile.length == 0 || /^\s+$/.test(emailSignInMobile)) {
        $("#emailSignInRequired-mobile").css('display','block');
        validEmail = false;
    }

    passSignIn = document.getElementById("passSignIn-mobile").value;
    if ( passSignIn == null || passSignIn.length == 0 || /^\s+$/.test(passSignIn)) {
        $("#passSignInRequired-mobile").css('display','block');
        validPass = false;
    }

    return validEmail && validPass;

}

function validationRegisterMobile() {
    var validNameMobile = true, validEmailRegisterMobile = true, validPhoneMobile = true, validPasswordMobile = true;

    nameRegister = document.getElementById("name-mobile").value;
    if ( nameRegister == null || nameRegister.length == 0) {
        $("#nameRequired-mobile").css('display','block');
        validNameMobile = false;
    }
    else {
        if (! /^[A-Za-z]+$/.test(nameRegister)) {
            $("#namePattern-mobile").css('display','block');
            validNameMobile = false;
        }
        else{
            $("#namePattern-mobile").css('display','none');
        }
        $("#nameRequired-mobile").css('display','none');
    }

    email = document.getElementById("email-mobile").value;
    if ( email == null || email.length == 0) {
        $("#emailRequired-mobile").css('display','block');
        validEmailRegisterMobile = false;
    }
    else {
        if (! /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
            $("#emailPattern-mobile").css('display','block');
            validEmailRegisterMobile = false;
        }
        else{
            $("#emailPattern-mobile").css('display','none');
        }
        $("#emailRequired-mobile").css('display','none');
    }

    phone = document.getElementById("phone-mobile").value;
    if ( phone == null || phone.length == 0) {
        $("#phoneRequired-mobile").css('display','block');
        validPhoneMobile = false;
    }
    else {
        if (! /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(phone)) {
            $("#phonePattern-mobile").css('display','block');
            validPhoneMobile = false;
        }
        else{
            $("#phonePattern-mobile").css('display','none');
        }
        $("#phoneRequired-mobile").css('display','none');
    }

    password = document.getElementById("password-mobile").value;
    if ( password == null || password.length == 0) {
        $("#passwordRequired-mobile").css('display','block');
        validPasswordMobile = false;
    }
    else {
        if (! /^(?=.*?[A-Z])(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[^\w\s]).{7,}$/.test(password)) {
            $("#passwordPattern-mobile").css('display','block');
            validPasswordMobile = false;
        }
        else{
            $("#passwordPattern-mobile").css('display','none');
        }
        $("#passwordRequired-mobile").css('display','none');
    }

    return validNameMobile && validEmailRegisterMobile && validPhoneMobile && validPasswordMobile;

}