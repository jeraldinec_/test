function mostrar() {
    document.getElementById("sidebar").style.width = "300px";
}

function ocultar() {
    document.getElementById("sidebar").style.width = "0";
}

function cascade() {
    $("#cascade li").css({
        left: -800,
        position: "relative"
    }).each(function (i) {
        var el = $(this);
        setTimeout(function () {
            el.animate({
                left: 0
            }, 500);
        }, i * 200);
    });
}

(function ($, window, undefined) {

    'use strict';

    $.HoverDir = function (options, element) {
        this.$el = $(element);
        this._init(options);
    };
    $.HoverDir.defaults = {
        speed: 300,
        easing: 'ease',
        hoverDelay: 0,
        inverse: false
    };

    $.HoverDir.prototype = {
        _init: function (options) {
            this.options = $.extend(true, {}, $.HoverDir.defaults, options);
            this.transitionProp = 'all ' + this.options.speed + 'ms ' + this.options.easing;
            this.support = Modernizr.csstransitions;
            this._loadEvents();
        },
        _loadEvents: function () {
            var self = this;
            this.$el.on('mouseenter.hoverdir, mouseleave.hoverdir', function (event) {
                var $el = $(this),
                    $hoverElem = $el.find('div'),
                    direction = self._getDir($el, {x: event.pageX, y: event.pageY}),
                    styleCSS = self._getStyle(direction);
                if (event.type === 'mouseenter') {
                    $hoverElem.hide().css(styleCSS.from);
                    clearTimeout(self.tmhover);
                    self.tmhover = setTimeout(function () {

                        $hoverElem.show(0, function () {

                            var $el = $(this);
                            if (self.support) {
                                $el.css('transition', self.transitionProp);
                            }
                            self._applyAnimation($el, styleCSS.to, self.options.speed);

                        });
                    }, self.options.hoverDelay);
                }
                else {
                    if (self.support) {
                        $hoverElem.css('transition', self.transitionProp);
                    }
                    clearTimeout(self.tmhover);
                    self._applyAnimation($hoverElem, styleCSS.from, self.options.speed);
                }
            });
        },
        _getDir: function ($el, coordinates) {
            var w = $el.width(),
                h = $el.height(),
                x = ( coordinates.x - $el.offset().left - ( w / 2 )) * ( w > h ? ( h / w ) : 1 ),
                y = ( coordinates.y - $el.offset().top - ( h / 2 )) * ( h > w ? ( w / h ) : 1 ),

                direction = Math.round(( ( ( Math.atan2(y, x) * (180 / Math.PI) ) + 180 ) / 90 ) + 3) % 4;
            return direction;
        },
        _getStyle: function (direction) {
            var fromStyle, toStyle,
                slideFromTop = {left: '0px', top: '-100%'},
                slideFromBottom = {left: '0px', top: '100%'},
                slideFromLeft = {left: '-100%', top: '0px'},
                slideFromRight = {left: '100%', top: '0px'},
                slideTop = {top: '0px'},
                slideLeft = {left: '0px'};
            switch (direction) {
                case 0:
                    // from top
                    fromStyle = !this.options.inverse ? slideFromTop : slideFromBottom;
                    toStyle = slideTop;
                    break;
                case 1:
                    // from right
                    fromStyle = !this.options.inverse ? slideFromRight : slideFromLeft;
                    toStyle = slideLeft;
                    break;
                case 2:
                    // from bottom
                    fromStyle = !this.options.inverse ? slideFromBottom : slideFromTop;
                    toStyle = slideTop;
                    break;
                case 3:
                    // from left
                    fromStyle = !this.options.inverse ? slideFromLeft : slideFromRight;
                    toStyle = slideLeft;
                    break;
            }
            ;
            return {from: fromStyle, to: toStyle};
        },
        _applyAnimation: function (el, styleCSS, speed) {
            $.fn.applyStyle = this.support ? $.fn.css : $.fn.animate;
            el.stop().applyStyle(styleCSS, $.extend(true, [], {duration: speed + 'ms'}));
        },
    };

    var logError = function (message) {
        if (window.console) {
            window.console.error(message);
        }
    };

    $.fn.hoverdir = function (options) {
        var instance = $.data(this, 'hoverdir');
        if (typeof options === 'string') {
            var args = Array.prototype.slice.call(arguments, 1);
            this.each(function () {
                if (!instance) {
                    logError("cannot call methods on hoverdir prior to initialization; " +
                        "attempted to call method '" + options + "'");
                    return;
                }
                if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {
                    logError("no such method '" + options + "' for hoverdir instance");
                    return;
                }
                instance[options].apply(instance, args);
            });
        }
        else {
            this.each(function () {
                if (instance) {
                    instance._init();
                }
                else {
                    instance = $.data(this, 'hoverdir', new $.HoverDir(options, this));
                }
            });
        }
        return instance;
    };
})

(jQuery, window);

(function ($, window, document, undefined) {
    'use strict';

    var deafults = {

        containerWidth: 0,
        containerHeight: 0,
        nOfRow: 0,
        nOfColumn: 0,
        aspectRatio: '1:1',
        containerMargin: '0px auto',
        itemWidth: 0,
        itemHeight: 0,
        itemDistance: 20,
        animationSpeed: 300,
        containerAnimationDelay: 500,
        allContainerAnimationSpeed: 500,
        easing: 'swing',
        margin: 0,
        filterController: '.filter-btn',
        responsive: [
            {
                breakpoint: 1200,
                containerWidth: 1170,
                settings: {
                    nOfRow: 5,
                    nOfColumn: 3
                }
            },
            {
                breakpoint: 992,
                containerWidth: 970,
                settings: {
                    nOfRow: 5,
                    nOfColumn: 3
                }
            },
            {
                breakpoint: 768,
                containerWidth: 750,
                settings: {
                    nOfRow: 8,
                    nOfColumn: 2
                }
            },
            {
                breakpoint: 767,
                containerWidth: 750,
                settings: {
                    nOfRow: 15,
                    nOfColumn: 1
                }
            }
        ]

    }

    function filterData(element, options) {

        this.settings = $.extend({}, deafults, options);
        this.element = $(element);
        this.win = $(window).width();
        this.all = this.element.find('>div');
        this.totalItems = this.all.length;
        var self = this;

        $(window).on('resize', function () {
            self.win = $(this).width();
            self.setScreen();
        });

        this.init();

    };

    filterData.prototype = {

        init: function () {
            var self = this;
            if (self.win >= 1200) {
                self.setSettings(1200);
            } else if (self.win >= 992) {
                self.setSettings(992);
            } else if (self.win >= 768) {
                self.setSettings(768);
            } else if (self.win < 768) {
                self.setSettings('xtra-small');
            }

            self.itemWidth = (self.settings.itemWidth == 0) ? self.containerWidth / self.nOfColumn : self.settings.itemWidth;
            self.itemHeight = self.getHeightFromRatio();
            self.containerHeight = (self.settings.containerHeight == 0) ? self.nOfRows * self.itemHeight : self.settings.containerHeight;
            self.top = 0,
                self.j = 0,
                self.k = 0;
            self.element.css({
                width: self.containerWidth,
                height: self.containerHeight,
                position: 'relative',
                overflow: 'hidden',
                margin: '0px auto'
            });
            self.all.each(function (i, item) {
                if (self.j * self.itemWidth >= self.element.width()) {
                    self.j = 0;
                    self.k++;
                }
                $(this).css({
                    width: self.itemWidth,
                    height: self.itemHeight,
                    position: 'absolute',
                    left: (self.j * self.itemWidth),
                    top: self.k * self.itemHeight,
                    paddingTop: self.settings.itemDistance
                });
                self.j++;
            });
            self.all.find('figure').css({
                width: '100%',
                height: '100%'
            });
            self.all.find('img').css({
                width: '100%',
                height: '100%'
            });
            $(self.settings.filterController).on('click', function (event) {
                self.$item = $(this).data('filter');
                if (undefined !== self.$item) {
                    $(this).addClass('active-filter');
                    self.items = self.element.find('div' + self.$item);
                    self.totalItems = self.items.length;
                    self.m = 0;
                    self.n = 0;
                }
                if ($(this).hasClass('active-filter')) {
                    self.controllDisplay();
                }
            });
        },
        controllDisplay: function () {
            switch (this.$item) {
                case 'all':
                    this.displayAll();
                    break;
                default :
                    this.displaySingleCategory();
                    break;
            }
        },
        setScreen: function () {
            var self = this;
            if (self.win >= 1200) {
                self.setSettings(1200);
            } else if (self.win >= 992) {
                self.setSettings(992);
            } else if (self.win >= 768) {
                self.setSettings(768);
            } else if (self.win < 768) {
                self.setSettings('xtra-small');
            }

            self.itemWidth = (self.settings.itemWidth == 0) ? self.containerWidth / self.nOfColumn : self.settings.itemWidth;
            self.itemHeight = self.getHeightFromRatio();
            self.top = 0,
                self.j = 0,
                self.k = 0;
            self.containerHeight = (self.settings.containerHeight == 0) ? self.nOfRows * self.itemHeight : self.settings.containerHeight;

            self.element.css({
                width: self.containerWidth,
                height: self.containerHeight,
                position: 'relative',
                overflow: 'hidden',
            });

            $(self.settings.filterController).each(function () {
                self.$item = $(this).data('filter');
                if ($(this).hasClass('active-work')) {
                    self.items = self.element.find('div' + self.$item);
                    self.totalItems = self.items.length;
                    self.m = 0;
                    self.n = 0;
                    self.controllDisplay();
                }

            });
        },
        setSettings: function (breakpoint) {
            var self = this;
            $.each(self.settings.responsive, function (index, responsive) {
                if (responsive.breakpoint == breakpoint) {
                    self.nOfColumn = responsive.settings.nOfColumn;
                    self.nOfRows = responsive.settings.nOfRow;
                    self.containerWidth = responsive.containerWidth;
                }
            });
            if (breakpoint == 'xtra-small') {
                self.containerWidth = self.win;
                self.nOfColumn = 1;
                self.nOfRows = 15;
            }
        },
        getHeightFromRatio: function () {
            this.ratio = this.settings.aspectRatio.split(':');
            return Math.ceil((this.itemWidth * this.ratio[1]) / this.ratio[0]);
        },
        displayAll: function () {
            var self = this;
            self.all.css({
                display: 'block',
                opacity: 1,
                transform: 'scale(1)',
                transition: 'all 0.5s ease'
            });
            self.all.each(function (i, item) {
                if (self.m * self.itemWidth >= self.element.width()) {
                    self.m = 0;
                    self.n++;
                }
                $(this).css({
                    display: 'block',
                    opacity: 1,
                    width: self.itemWidth,
                    height: self.itemHeight,
                    position: 'absolute',
                }).animate({
                        left: (self.m * self.itemWidth),
                        top: self.n * self.itemHeight
                    },
                    self.settings.animationSpeed,
                    self.settings.easing);
                self.m++;
            });
            self.element
                .delay(self.settings.containerAnimationDelay)
                .animate({
                    height: self.containerHeight
                }, self.settings.allContainerAnimationSpeed);
        },
        displaySingleCategory: function () {
            var self = this,
                Rows = Math.ceil(self.totalItems / self.nOfColumn),
                containerHeight;
            if (Rows <= self.nOfRows) {
                containerHeight = self.itemHeight * Rows;
            } else {
                containerHeight = self.itemHeight * self.nOfRows;
            }
            if (self.$item != undefined) {
                // Hide all if not match current items
                self.all.each(function (i, item) {
                    if (!self.matchCurrent(item)) {
                        $(this).css({
                            display: 'block',
                            opacity: 0,
                            transform: 'scale(0)',
                            transition: 'all 0.5s ease'
                        });
                    }
                });
                self.items.css({
                    display: 'block',
                    opacity: 1,
                    transform: 'scale(1)',
                    transition: 'all 0.5s ease',
                });
                $(self.items).each(function (i, item) {
                    if (self.m * self.itemWidth >= self.element.width()) {
                        self.m = 0;
                        self.n++;
                    }
                    $(item).css({
                        display: 'block',
                        opacity: 1,
                        width: self.itemWidth,
                        height: self.itemHeight,
                        position: 'absolute',
                    });
                    $(this).animate({
                            left: self.m * self.itemWidth,
                            top: self.n * self.itemHeight
                        },
                        self.settings.animationSpeed,
                        self.settings.easing);
                    self.m++;
                });
                self.element.delay(self.settings.containerAnimationDelay)
                    .animate({
                        height: containerHeight
                    }, self.settings.allContainerAnimationSpeed);
            }
        },
        matchCurrent: function (current) {
            var currentItems = $.map(self.items, function (value, index) {
                return [value];
            });
            for (var i = 0; i < currentItems.length; i++) {
                if (current === currentItems[i]) {
                    return true;
                }
            }
            return false;
        }
    };

    $.fn.filterData = function (options) {

        return this.each(function () {

            if (!$.data(this, 'filterData')) {

                $.data(this, 'filterData', new filterData(this, options));
            }
        });
    };


})(jQuery, window, document);

$(document).ready(function(){
    $('body').scrollspy({target: ".navbar", offset: 50});
    $("#scroll a").on('click', function (event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function () {
                window.location.hash = hash;
            });
        }
    });

    $(' .da-thumbs > li ').each( function() { $(this).hoverdir(); } );

    $('.portfolios').filterData({
        aspectRatio: '8:5',
        nOfRow : 3,
        itemDistance : 0
    });
    $('.portfolio-controllers button').on('click',function(){
        $('.portfolio-controllers button').removeClass('active-work');
        $(this).addClass('active-work');
    });

    $("#right").click(function(){
        $(".modal-options").animate({"left": "+=50%"});
        $("#create").text("Become part of our community");
        $("#already").text("Already have an account?");
        $('#register').addClass("hide");
        $('#signin').removeClass("hide");
    });

    $("#left").click(function(){
        $(".modal-options").animate({"left": "-=50%"});
        $("#create").text("Welcome Back!");
        $("#already").text("Don't have an account?");
        $('#signin').addClass("hide");
        $('#register').removeClass("hide");
    });

    $("#bottom").click(function(){
        $(".modal-options").animate({"top": "+=50%"});
        $("#create-mobile").text("Become part of our community");
        $("#already-mobile").text("Already have an account?");
        $('#register-mobile').addClass("hide");
        $('#signin-mobile').removeClass("hide");
    });

    $("#top").click(function(){
        $(".modal-options").animate({"top": "-=50%"});
        $("#create-mobile").text("Welcome Back!");
        $("#already-mobile").text("Don't have an account?");
        $('#signin-mobile').addClass("hide");
        $('#register-mobile').removeClass("hide");
    });

    $(".fancybox").fancybox();

    $('#showPass').click(function() {
        $('#password').attr('type', 'text');
        $('#showPass').css('display', 'none');
        $('#hidePass').css('display', 'block');
    });

    $('#hidePass').click(function() {
        $('#password').attr('type', 'password');
        $('#showPass').css('display', 'block');
        $('#hidePass').css('display', 'none');
    });

    $('#showPass-mobile').click(function() {
        $('#password-mobile').attr('type', 'text');
        $('#showPass-mobile').css('display', 'none');
        $('#hidePass-mobile').css('display', 'block');
    });

    $('#hidePass-mobile').click(function() {
        $('#password-mobile').attr('type', 'password');
        $('#showPass-mobile').css('display', 'block');
        $('#hidePass-mobile').css('display', 'none');
    });

    if ($(window).innerWidth() <= 767) {
        console.log('jeral esta lcoa')
        $("#login").modal("hide");
    }

});
